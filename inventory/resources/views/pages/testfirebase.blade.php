<!-- AngularJS -->
<script src="{https://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js}"></script>

<!-- Firebase -->
<script src="{https://www.gstatic.com/firebasejs/3.6.5/firebase.js}"></script>

<!-- AngularFire -->
<script src="{https://cdn.firebase.com/libs/angularfire/2.2.0/angularfire.min.js}"></script>

<script src="{{('js/appfirebase.js')}}"></script>


<html ng-app="sampleApp">
<body ng-controller="SampleCtrl">
<!-- anything typed in here is magically saved to our Firebase database! -->
<input type="text" ng-model="data.text"/>
<!-- all changes from our Firebase database magically appear here! -->
<h1>You said: {{ data.text }}</h1>
</body>
</html>