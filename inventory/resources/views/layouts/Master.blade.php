<!DOCTYPE html>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script href="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script href="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><html class="no-js"> <!--<![endif]-->
<html lang="en">


<head >

    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <title>@yield('title') </title>
    <!-- Bootstrap Core CSS -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
    <link rel="stylesheet" href="{{('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{('css/normalize.min.css')}}">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{('css/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="{{('js/appfirebase.js')}}"></script>

    <!-- AngularJS -->
    <script src="{{('https://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js')}}"></script>

    <!-- Firebase -->
    <script src="{{('https://www.gstatic.com/firebasejs/3.6.5/firebase.js')}}"></script>

    <!-- AngularFire -->
    <script src="{{('https://cdn.firebase.com/libs/angularfire/2.2.0/angularfire.min.js')}}"></script>





    <script src="{{('https://www.gstatic.com/firebasejs/3.6.2/firebase-database.js')}}"></script>



    <!-- Theme style -->
    <link rel="stylesheet" href="{{('css/animate.css')}}">
    <link rel="stylesheet" href="{{('css/templatemo-misc.css')}}">
    <link rel="stylesheet" href="{{('css/templatemo-style.css')}}">





    @yield('styles')
    <style>

    </style>
</head>
<body >

@include('partials.header')

@yield('content')

@include('partials.footer')


</body>


</html>
