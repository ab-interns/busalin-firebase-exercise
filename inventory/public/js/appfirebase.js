(function () {


    var config = {
        apiKey: "AIzaSyClPTChlBpzEaHkZR4N0eYj5iWf-nOJ7cQ",
        authDomain: "testfirebase-2d7a4.firebaseapp.com",
        databaseURL: "https://testfirebase-2d7a4.firebaseio.com",
        storageBucket: "testfirebase-2d7a4.appspot.com",
        messagingSenderId: "407932652296"
    };
    firebase.initializeApp(config);

    //การเข้าถึง Element Id ต่าง ๆ ใน Form HTML ใน Attribue Id
    const preObject = document.getElementById('object');
    //Create references
    const dbRefObject = firebase.database().ref().child('object');

    //Synnc object changes (ต้องการเปลี่ยน ซินแทคในออฟเจคนั้น)
    dbRefObject.on('value',snap = console.log(snap.val()));



    var app = angular.module("sampleApp", ["firebase"]);
    app.controller("SampleCtrl", function($scope, $firebaseObject) {
        var ref = firebase.database().ref().child("data");
        // download the data into a local object
        var syncObject = $firebaseObject(ref);
        // synchronize the object with a three-way data binding
        // click on `index.html` above to see it used in the DOM!
        syncObject.$bindTo($scope, "data");
    });
});